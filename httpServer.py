from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import cgi
import time
import socket
import urllib
import hashlib
import threading
import http.client
import urllib.parse
import urllib.request
import concurrent.futures
import ast
from queue import Queue

hostPort = 9000
socket.setdefaulttimeout(1)
__iplist__ = []
__url__ = ''
__sent_id__ = ''

class MyServer(BaseHTTPRequestHandler):
    
    def do_GET(self):
        parsed_path = urllib.parse.urlparse(self.path)

        if(parsed_path.path == '/resource'):
            self.send_response(200)
            self.end_headers()
            MyServer.handle_resource_request(parsed_path.query)
            return

        self.send_response(404)
        self.end_headers()
        return

    def do_POST(self):
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,          
            environ={'REQUEST_METHOD':'POST','CONTENT_TYPE':self.headers['Content-Type']})
        
        if (self.path == '/resourcereply'):
            self.send_response(200)
            self.end_headers()
            print("Resource reply recieved")
            MyServer.send_md5_request(form)
            return
        elif (self.path == '/checkmd5'):
            self.send_response(200)
            self.end_headers()
            print("Check md5 request recieved")
            MyServer.start_md5(form)
            return
        elif (self.path == '/answermd5'):
            self.send_response(200)
            self.end_headers()
            print("Answer md5 request recieved")
            MyServer.handle_md5_response(form)
            return
        
        self.send_response(404)
        self.end_headers()
        return

    def handle_resource_request(query):
        query_components = urllib.parse.parse_qs(query)
        sent_ip = str(query_components['sendip'].pop())
        sent_port = str(query_components['sendport'].pop())
        ttl = int(query_components['ttl'].pop())
        MyServer.__sent_id__ = str(query_components['id'].pop())
        MyServer.__url__ = 'http://' + sent_ip + ':' + sent_port + '/'
        noask = []
        for ip in query_components['noask']:
            noask.append(ip)

        if ttl > 0:
            ttl -= 1
            noask.append(__myip__)
            for ip in __iplist__:
                if ip not in noask:
                    url = 'http://' + ip + ':' + sent_port + '/resource?sendip=' + sent_ip + '&sentport=' + sent_port + '&ttl=' + str(ttl) + '&id=' + __sent_id__
                    for noask_ip in noask:
                        query += '&noask=' + noask_ip
                    MyServer.send(url)
        if True: #have_resource
            MyServer.resource_reply()
        return

    def resource_reply():
        print("__url__ - %s" %MyServer.__url__)
        url = MyServer.__url__ + "resourcereply"
        print ("Sending response to url %s" % url)
        data = urllib.parse.urlencode({"ip": __myip__, 
           "port": hostPort,
           "id": MyServer.__sent_id__,
           "resource": 100}).encode('utf-8')
        print("Sending resource reply")
        MyServer.send(url, data)
        return

    def send(url, data):
        req = urllib.request.Request(url);
        req.add_header("Content-Type","application/x-www-form-urlencoded;charset=utf-8")
        req.add_header("Content-Length","32")
        req.add_header("User-Agent","Tristan")
        try:
            if data:
                f = urllib.request.urlopen(req, data)
            else:
                f = urllib.request.urlopen(req)
            f.close()
        except:
            print("Timeout")
        return

    def send_md5_request(form):
        ip = form['ip'].value
        port = form['port'].value
        data = urllib.parse.urlencode({
            "ip": __myip__, 
            "port": hostPort,
            "id": "siinonid",
            "md5": "68e1c85222192b83c04c0bae564b493d",
            "ranges": ["ko?r"],
            "wildcard": "?",
            "symbolrange": [[3,10],[100,150]]
        }).encode('utf-8')
        url = "http://" + ip + ":" + port + "/checkmd5"
        print ("Sending md5 crack request to url %s" % url)
        MyServer.send(url, data)
        return

    def start_md5(form):
        hexhash = form['md5'].value
        template = form['ranges'].value
        template = ast.literal_eval(template)
        wildcard = form['wildcard'].value
        symbolrange = form['symbolrange'].value
        print("hexhash - %s" %hexhash)
        print("template - %s" %template)
        print("wildcard - %s" %wildcard)
        print("Processing...")
        for temp in template:
            res = MyServer.md5_crack(hexhash, temp, wildcard)
            MyServer.answer_md5(form, res)
        return

    def md5_crack(hexhash, template, wildcard):
        i = 0
        found = False
        while i < len(template):
            if template[i] == wildcard:  
                found = True
                char = 32 # start with this char ascii
                while char < 126:
                    c = chr(char)
                    #print(c)
                    if c != wildcard: # cannot check wildcard!        
                        ntemplate = template[:i] + c + template[i + 1:]
                        #print("i: " +str(i) + " ntemplate: " + ntemplate)
                        res = MyServer.md5_crack(hexhash, ntemplate, wildcard)
                        if res: # stop immediately if cracked
                            print("Done.")
                            return res
                    char += 1
            i += 1    
        # instantiation loop done  
        if not found:
            # no wildcards found in template: crack
            m=hashlib.md5()
            m.update(template.encode(encoding='UTF-8'))
            hash=m.hexdigest()
            #print("template: "+template+" hash: "+hash) 
            if hash==hexhash: 
                return template # cracked!
        # template contains wildcards
        return None

    def answer_md5(form, res):
        ip = form['ip'].value
        port = form['port'].value
        req_id = form['id'].value
        hexhash = form['md5'].value

        if res:
            result_status = 0
        else:
            result_status = 1

        data = urllib.parse.urlencode({
            "ip": __myip__, 
            "port": port,
            "id": req_id,
            "md5": hexhash,
            "result": result_status,
            "resultstring": res
        }).encode('utf-8')
        print("Sending md5 answer - %s" %res)
        url = MyServer.__url__ + 'answermd5'
        MyServer.send(url, data)
        return

    def handle_md5_response(form):
        ip = form['ip'].value
        port = form['port'].value
        sent_id = form['id'].value
        result = form['result'].value
        result_string = form['resultstring'].value
        if result == 0:
            print("Answer found by %s - %s" %(ip, result_string))
        else:
            print("%s did not find answer for request id=%s" %(ip, sent_id))
        return

# Get my ip
__myip__ = socket.gethostbyname(socket.gethostname())
if __myip__.startswith("127.") and os.name != "nt":
    interfaces = ["eth0", "eth1", "eth2", "wlan0", "wlan1", "wifi0", "ath0", "ath1", "ppp0"]
    for ifname in interfaces:
        try:
            __myip__ = get_interface_ip(ifname)
            print('My ip is: %s' % __myip__)
            break
        except IOError:
            pass

#__myip__ = 'localhost'
print('My ip is: %s' % __myip__)

myServer = HTTPServer((__myip__, hostPort), MyServer)
print(time.asctime(), "Server Starts - %s:%s" % (__myip__, hostPort))

try:
    print("Serving forever...")
    myServer.serve_forever()
except KeyboardInterrupt:
    pass

myServer.server_close()
print(time.asctime(), "Server Stops - %s:%s" % (__myip__, hostPort))
